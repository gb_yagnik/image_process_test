import pytest



# USAGE
# python buttons.py
# python buttons.py --video data.mp4

# import the necessary packages
from imutils.video import VideoStream
from imutils.video import FPS
import argparse
import imutils
import time
import cv2
import numpy as np

def analysis_button(frame):
	# global no
	grayFrame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	
	#apply gaussian blur
	grayFrame = cv2.GaussianBlur(grayFrame, (7, 7), 0)
	
	#threshold image
	ret,grayFrame = cv2.threshold(grayFrame,127,255,cv2.THRESH_OTSU+cv2.THRESH_BINARY)
	
	#show gray scale blurred images
	# cv2.resizeWindow("GFrame", (800, 800))
	cv2.namedWindow("GFrame",cv2.WINDOW_NORMAL)
	cv2.imshow("GFrame", grayFrame)
	
	#find contours
	_,cnts, he = cv2.findContours(grayFrame.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
	
	# if we get more than one contours draw and fill the inner part of contour
	if len(cnts) > 0:
		Frame = frame.copy()
		for i in range(0,len(cnts) - 1):
			cv2.drawContours(frame, cnts[i], -1, (0,255,0), -1)
			cv2.fillPoly(Frame, pts =[cnts[i]], color=(255,255,255))
			grayFrame_test = cv2.fillPoly(grayFrame.copy(), pts=[cnts[i]], color=255)

	#size is used to remove small contours
	cv2.namedWindow("test",cv2.WINDOW_NORMAL)
	cv2.imshow("test", grayFrame_test)
	cv2.waitKey(0)
	size = 150000
	_,cnts, he = cv2.findContours(grayFrame_test, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
	cnts_sorted = sorted(cnts,key=cv2.contourArea,reverse=True)
	cnts_sorted = cnts_sorted[:10]
	# print("COUNTOURSSS",len(cnts))
	#Process all contours in the frame
	circle_percent = 0
	for i in range(0,len(cnts_sorted) - 1):
		
		#Find size of contour
		(x, y, w, h) = cv2.boundingRect(cnts[i])

		#Find Min enclosing circle that fits contour
		((centerX, centerY), radius) = cv2.minEnclosingCircle(cnts[i])
		
		#used to make a decision
		bool = 0
		# print()
		#if centre of button comes within a fixed range mentioned below than only it will be processed
		if centerY >= (3*1080)/7 and centerY <= (5*1080)/7 and h*w > size:
			#print(centerY)
			print("size",h*w)
			#increment one frame processed
			# no+=1
			# print("NO",no)
			# print(no)
			
			#used to make a decision
			bool = 1
			
			#Radius of button in pixels
			print("RADIUS",radius)
			# print()
			
			#size of 1st button in mm
			mm = 110
			#if button is 1st button then map button size in mm to pixels 
			# if no == 1:
			radius_check = radius
			ratioMM = mm / radius
			print("RATIOMM",ratioMM)
				# print(ratioMM)
			
			#5% error can be tolerated
			if 0.95 * ratioMM * radius_check < ratioMM * radius and 1.05 * ratioMM * radius_check > ratioMM * radius:
				print("PASS")

		#separate button from frame
		button = Frame[y:y + h, x:x + w]
		
		#if Height and Width of separated part is greater than size
		if h*w > size:
			
			#make a canvas of same size of contour of button
			canvas = np.zeros((button.shape[0], button.shape[1], 3), dtype = "uint8")
			black = (255, 255, 255)
			
			#find centre of canvas
			(centerX, centerY) = (canvas.shape[1] // 2, canvas.shape[0] // 2)
			
			#radius of circle which best fits canvas
			radius = centerX if(centerX <= centerY) else centerY
			
			#draw a circle on canvas
			cv2.circle(canvas, (centerX, centerY), radius, black, -1)
			# cv2.imshow("winname", canvas)
			# cv2.waitKey(0)
			#Form a mask and compare thresholded image of button with generated circle on canvas
			mask = np.zeros(frame.shape[:2], dtype = "uint8")

			((centerX, centerY), radius) = cv2.minEnclosingCircle(cnts[i])
			cv2.circle(mask, (int(centerX), int(centerY)), int(radius), 255, -1)
			mask = mask[y:y + h, x:x + w]
			Final = cv2.bitwise_and(button, button, mask = mask)
			#show masked image
			cv2.imshow("FINAL ",Final)

			#pos and neg corresponds to positive and negative results of masks 
			pos = 0
			neg = 0
			
			#get results and save in boolean
			boolean = (cv2.cvtColor(Final, cv2.COLOR_BGR2GRAY) - cv2.cvtColor(canvas, cv2.COLOR_BGR2GRAY))
			
			
			boolean[boolean > 0] = 2
			
			neg = (boolean.sum(0).sum(0) ) / 2
			
			#boolean[boolean > 0] = 2
			boolean[boolean == 0] = 1
			boolean[boolean == 2] = 0
			
			pos = boolean.sum(0).sum(0)
			
			#print(neg)
			#print(pos)
			
			"""
			pos1 = 0
			neg1 = 0
			boolean = (cv2.cvtColor(Final, cv2.COLOR_BGR2GRAY) - cv2.cvtColor(canvas, cv2.COLOR_BGR2GRAY))
			for i in range(canvas.shape[0]):
				for j in range(canvas.shape[1]):
					if(boolean[i][j] == 0):
						pos1 += 1
					else:  
						neg1 += 1
			
			print("TTT")
			print(neg1)
			print(pos1)
			
			if(neg == neg1):
				print(")))))))))))))))))))))))))))))))))")
			if(pos == pos1):
				print("((((((((((((((((((((((((((((((((((")
			"""
			
			if(bool == 1):
				#percentage cirle
				print("Percentage circle",(pos / (pos + neg)) * 100 )
				# print()
				circle_percent = (pos / (pos + neg)) * 100 
			
			
		
			#select inner 1/2 of button and save in colour_button 
			colour_button = frame[y + int(h/4):y + int(3*(h)/4), x + int(w/4):x + int(3*(w)/4)]
			
			#convert rgb to gray
			colour_button = cv2.cvtColor(colour_button, cv2.COLOR_BGR2GRAY)
			
			#set threshold and threshold the inmage
			thresh = 100
			
			colour_button[colour_button > thresh] = 255
			colour_button[colour_button < thresh] = 0

			button_centre = colour_button.copy()
			
			#find contours on thresholded image
			_,contours, hierarchy = cv2.findContours(button_centre, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
			
			#if we have more than one contours print no of contours
			if len(contours) > 0 and bool == 1:
				print("contours",len(contours)-1)   # length - 1 is used to remove boundary as a contour
			
			#used for display convert gray scale to rgb
			button_centre = cv2.cvtColor(button_centre, cv2.COLOR_GRAY2RGB)
			
			#used for display draws contour boundary
			for j in range(0,len(contours)):
				cv2.drawContours(button_centre, contours[j], -1, (0,255,0), 3)
			
			#show holes in buttons
			
	return circle_percent

		####################
# construct the argument parser and parse the arguments
def main():
	ap = argparse.ArgumentParser()
	ap.add_argument("-v", "--video", type=str,default="./data.mp4",help="path to input video file")

	args = vars(ap.parse_args())


	# # if a video path was not supplied, grab the reference to the web cam
	# if not args.get("video", False):
	# 	print("[INFO] starting video stream...")
	# 	vs = VideoStream(src=0).start()
	# 	time.sleep(1500.0)

	# otherwise, grab a reference to the video file
	# else:
	vs = cv2.VideoCapture(args["video"])

	# initialize the FPS throughput estimator ie if value_fps = 5 than 1 out of 5 frames will be processed
	value_fps = 5

	#fps is a variable use to drop frames
	fps = 0

	#no of frames processed
	no = 0

	# loop over frames from the video stream
	while True:
		
		fps += 1
			
		#process frame
		if fps == value_fps:
			frame = vs.read()
			fps = 0
			
		#drop frame by continuing
		else:
			frame = vs.read()
			continue

		frame = frame[1] if args.get("video", False) else frame


		# check to see if we have reached the end of the stream
		if frame is None:
			#print("END")
			break

		#convert data frame from rgb to gray scale
		cir_per = analysis_button(frame)
		#timer if we capture data from webcam	
		# time.sleep(1)
		key = cv2.waitKey(500) & 0xFF

		
		# if the `q` key was pressed, break from the loop
		if key == ord("q"):
			break

	# if we are using a webcam, release the pointer
	if not args.get("video", False):
		vs.stop()

	# otherwise, release the file pointer
	else:
		vs.release()

cv2.destroyAllWindows()

if __name__ == '__main__':
	main()



































